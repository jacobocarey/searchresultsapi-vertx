package com.jdw.searchresults.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Product {

    private int productId;

    private String description;

    private BigDecimal price;

    private String imageUrl;

    private boolean inSale;

    public Product(@JsonProperty("productId") Integer productId, @JsonProperty("description") String description,
            @JsonProperty("price") BigDecimal price, @JsonProperty("imageUrl") String imageUrl,
            @JsonProperty("inSale") boolean inSale) {
        this.productId = productId;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
        this.inSale = inSale;
    }

    public Integer getProductId() {
        return productId;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isInSale() {
        return inSale;
    }

}
