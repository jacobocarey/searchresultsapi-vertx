package com.jdw.searchresults.verticles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdw.searchresults.ProductDataStub;
import com.jdw.searchresults.model.Product;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ProductVerticle extends AbstractVerticle {

    // Move to properties file.
    private String defaultPageSize = "48";

    @Override
    public void start(Future<Void> future) {

        Router router = Router.router(vertx);
        router.get("/products").handler(this::getAllProducts);
        router.get("/products/:productId").handler(this::getProduct);

        vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080),
                result -> {
                    if (result.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(result.cause());
                    }
                });
    }

    public void getAllProducts(RoutingContext routingContext) {
        List<Product> allProducts = new ArrayList<>();
        // Order list for valid pagination.
        Stream<ProductDataStub> productDataStubStream = Arrays.stream(ProductDataStub.values());
        productDataStubStream.forEachOrdered(productDataStub -> allProducts.add(productDataStub.getProduct()));
        List<Product> products = allProducts;

        String page = routingContext.request().getParam("page");
        if (page != null) {
            String productsPerPage = routingContext.request().getParam("productsPerPage");
            productsPerPage = productsPerPage == null ? defaultPageSize : productsPerPage;
            products = getProductsForPage(allProducts, page, productsPerPage);
        }
        try {
            routingContext.response().putHeader("content-type", "application/json")
                    .end(new ObjectMapper().writeValueAsString(products));
        } catch (JsonProcessingException e) {
            routingContext.response().end("Failing to parse object to JSON.");
        }
    }

    public void getProduct(RoutingContext routingContext) {
        String productId = routingContext.request().getParam("productId");
        if (productId == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Stream<ProductDataStub> productDataStubStream = Arrays.stream(ProductDataStub.values());
            ProductDataStub matchedProductDataStub = productDataStubStream.filter(
                    productDataStub -> (productDataStub.getProduct().getProductId() == Integer.valueOf(productId)))
                    .findAny().orElse(null);
            if(matchedProductDataStub == null) {
                routingContext.response().end("No product exists with ID: " + productId);
            } else {
                Product product = matchedProductDataStub.getProduct();
                try {
                    routingContext.response().putHeader("content-type", "application/json")
                            .end(new ObjectMapper().writeValueAsString(product));
                } catch (JsonProcessingException e) {
                    routingContext.response().end("Failing to parse object to JSON.");
                }
            }
        }
    }



    /**
     * Will return products for the selected page, if selected page it too high then return empty list.
     *
     * @param page,
     *            number of the page we want to retrieve.
     * @param allProducts,
     *            list of all the products.
     * @return Ordered list of products for selected page or empty if requested page is too high.
     */
    private List<Product> getProductsForPage(List<Product> allProducts, String page, String productsPerPage) {
        List<Product> productsForPage = new ArrayList<>();
        int pageSize = Integer.parseInt(productsPerPage);
        int pageNumber = Integer.parseInt(page);

        Integer fromIndex = pageSize * (pageNumber - 1) > allProducts.size() ? null : pageSize * (pageNumber - 1);
        if (fromIndex != null && fromIndex >= 0 && pageSize > 0) {
            int toIndex = pageSize * pageNumber >= allProducts.size() ? allProducts.size() : pageSize * pageNumber;
            productsForPage = allProducts.subList(fromIndex, toIndex);
        }
        return productsForPage;
    }
}
