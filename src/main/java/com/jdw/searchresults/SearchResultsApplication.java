package com.jdw.searchresults;

import com.jdw.searchresults.verticles.ProductVerticle;
import io.vertx.core.Vertx;

public class SearchResultsApplication {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ProductVerticle());

        /**
         * Not is use.
         *
         * vertx.deployVerticle(new ServerVerticle());
         *
         */
    }
}
