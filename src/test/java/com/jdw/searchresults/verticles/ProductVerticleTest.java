package com.jdw.searchresults.verticles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdw.searchresults.ProductDataStub;
import com.jdw.searchresults.model.Product;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;

@RunWith(VertxUnitRunner.class)
public class ProductVerticleTest {

    private Vertx vertx;

    @Before
    public void setUp(TestContext context) {
        vertx = Vertx.vertx();
        vertx.deployVerticle(ProductVerticle.class.getName(), context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void givenAProductIdAsAPathVariableWhenMakingARequestForASingleProductThenExpectAValidResponse(
            TestContext context) {
        final Async async = context.async();

        vertx.createHttpClient().getNow(8080, "localhost", "/products/1", response -> {
            response.handler(body -> {
                try {
                    context.assertTrue(body.toString()
                            .equals(new ObjectMapper().writeValueAsString(ProductDataStub.PRODUCT1.getProduct())));
                } catch (JsonProcessingException e) {
                    fail("JsonProcessingException was thrown.");
                }
                async.complete();
            });
        });
    }

    @Test
    public void
            givenAProductIdThatDoesNotExistAaAPathVariableWhenMakingARequestForASingleProductThenExpectAnInvalidResponse(
                    TestContext context) {
        final Async async = context.async();

        vertx.createHttpClient().getNow(8080, "localhost", "/products/100", response -> {
            response.handler(body -> {
                context.assertTrue(body.toString().equals("No product exists with ID: 100"));
                async.complete();
            });
        });
    }

    @Test
    public void givenQueryParamsWhenMakingARequestThenExpectAValidResponse(TestContext context) {
        final Async async = context.async();

        List<Product> products = new ArrayList<>();
        products.add(ProductDataStub.PRODUCT6.getProduct());
        products.add(ProductDataStub.PRODUCT7.getProduct());
        products.add(ProductDataStub.PRODUCT8.getProduct());
        products.add(ProductDataStub.PRODUCT9.getProduct());
        products.add(ProductDataStub.PRODUCT10.getProduct());

        vertx.createHttpClient().getNow(8080, "localhost", "/products?page=2&productsPerPage=5", response -> {
            response.handler(body -> {
                try {
                    context.assertTrue(body.toString().equals(new ObjectMapper().writeValueAsString(products)));
                } catch (JsonProcessingException e) {
                    fail("JsonProcessingException was thrown.");
                }
                async.complete();
            });
        });
    }
}
